import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class TestClass {
	static int stack[];
	static int top;
	

	public static void main(String[] args) throws IOException {
		TestClass tc = new TestClass();
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		try {
			int numberoftestcase = tc.testCaseValidator(br);
			for (int i = 0; i < numberoftestcase; i++) {
				int[] passDetail = tc.passValidation(br);
				stack = new int[passDetail[0] + 1];
				top = -1;
				push(passDetail[1]);
				for (int j = 0; j < passDetail[0]; j++) {
					String passes = tc.userIdValidation(br);
					if (passes.charAt(0) == 'B') {
						pushB();
					} else
						push(Integer.parseInt(passes.substring(2)));
				}
				System.out.println("Player " + stack[top]);
			}
		} catch (IllegalArgumentException iae) {
            System.out.println(iae.getMessage());
		}
		finally{
			br.close();
		}
		
	}

	public int testCaseValidator(BufferedReader br) throws NumberFormatException, IOException {
		int numberoftestcase = Integer.parseInt(br.readLine());
		if (numberoftestcase < 0 || numberoftestcase > 100) {
			throw new IllegalArgumentException("Invalid Number of Testcases");
		}
		return numberoftestcase;
	}

	public int[] passValidation(BufferedReader br) throws IOException {
		String pass[] = br.readLine().split(" ");
		int passDetail[] = new int[2];
		passDetail[0] = Integer.parseInt(pass[0]);
		passDetail[1] = Integer.parseInt(pass[1]);
		if (passDetail[0] < 0 || passDetail[0] > 100000) {
			throw new IllegalArgumentException("Invalid number of passes");
		}
		return passDetail;
	}

	public String userIdValidation(BufferedReader br) throws IOException {
		String passes = br.readLine();
		if (!(passes.charAt(0) == 'B')) {
			if (Integer.parseInt(passes.substring(2)) < 0 || Integer.parseInt(passes.substring(2)) > 1000000) {
				throw new IllegalArgumentException("Invalid User id");
			}
		}
		return passes;
	}

	private static void pushB() {
		stack[++top] = stack[top - 2];
	}

	private static void push(int parseInt) {
		stack[++top] = parseInt;
	}
}