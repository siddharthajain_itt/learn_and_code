import static org.junit.Assert.*;
import org.junit.Test;

public class TestMonkBreakingArrayIntoTree {

	// Naming Convention: MethodName_StateUnderTest_ExpectedBehavior

	@Test
	public void insertNewNode_AddNewNodeInTree_ReturnValueStoredInNewNode() {
		MonkBreakingArrayIntoTree tester = new MonkBreakingArrayIntoTree();

		assertEquals(5, tester.insertNewNode(5).value);
	}

	@Test
	public void findLocationToInsertNewNode_PositionOfNewNodeInsert_InsertNodeAsRoot() {
		MonkBreakingArrayIntoTree tester = new MonkBreakingArrayIntoTree();
		Node root = null;
		root = tester.findLocationToInsertNewNode(root, 5);

		assertNotNull(tester.root);
	}

	@Test
	public void findLocationToInsertNewNode_PositionOfNewNodeInsert_InsertNodeInLeft() {
		MonkBreakingArrayIntoTree tester = new MonkBreakingArrayIntoTree();
		Node root = null;
		root = tester.findLocationToInsertNewNode(root, 5);
		tester.findLocationToInsertNewNode(root, 4);

		assertEquals(4, root.leftsideofnode.value);
	}

	@Test
	public void findLocationToInsertNewNode_PositionOfNewNodeInsert_InsertNodeInRight() {
		MonkBreakingArrayIntoTree tester = new MonkBreakingArrayIntoTree();
		Node root = null;
		root = tester.findLocationToInsertNewNode(root, 5);
		tester.findLocationToInsertNewNode(root, 6);

		assertEquals(6, root.rightsideofnode.value);
	}

	@Test
	public void loadValueForInsertion_ToLoadKeyInRoot_LoadNodeAsRoot() {
		MonkBreakingArrayIntoTree tester = new MonkBreakingArrayIntoTree();
		tester.loadValueForInsertion(5);

		assertEquals(5, tester.root.value);
	}

	@Test
	public void isTreeHeightAvailable_StateOfQueue_True() {
		MonkBreakingArrayIntoTree tester = new MonkBreakingArrayIntoTree();

		assertTrue(tester.isTreeHeightAvailable());
	}

	@Test
	public void isTreeHeightAvailable_StateOfQueue_False() {
		MonkBreakingArrayIntoTree tester = new MonkBreakingArrayIntoTree();

		Node root = new Node(5);
		root.leftsideofnode = new Node(4);
		tester.queueofnode.add(root);
		tester.queueofnode.add(root.leftsideofnode);

		assertFalse(tester.isTreeHeightAvailable());
	}

	@Test
	public void getTreeHeight_ReturnValueOfVariableHeight_0() {
		MonkBreakingArrayIntoTree tester = new MonkBreakingArrayIntoTree();

		assertEquals(0, tester.getTreeHeight());
	}

	@Test
	public void increaseHeightOfTree_IncreaseValueOfVariableHeight_0() {
		MonkBreakingArrayIntoTree tester = new MonkBreakingArrayIntoTree();
		tester.increaseHeightOfTree();

		assertEquals(1, tester.height);
	}

	@Test
	public void iterateTreeForNodesInQueue_DecreaseSizeOfQueue_SizeOfQueueAs0() {
		MonkBreakingArrayIntoTree tester = new MonkBreakingArrayIntoTree();

		Node root = new Node(5);
		tester.queueofnode.add(root);
		tester.iterateTreeForNodesInQueue(1);

		assertEquals(0, tester.queueofnode.size());
	}

	@Test
	public void iterateTreeForNodesInQueue_AddLeftSideOfNodeInQueue_ValueLeftSideOfNode() {
		MonkBreakingArrayIntoTree tester = new MonkBreakingArrayIntoTree();

		Node root = new Node(5);
		root.leftsideofnode = new Node(4);
		tester.queueofnode.add(root);
		tester.iterateTreeForNodesInQueue(1);

		assertEquals(4, tester.queueofnode.peek().value);
	}

	@Test
	public void iterateTreeForNodesInQueue_AddRightSideOfNodeInQueue_ValueRightSideOfNode() {
		MonkBreakingArrayIntoTree tester = new MonkBreakingArrayIntoTree();

		Node root = new Node(5);
		root.leftsideofnode = new Node(6);
		tester.queueofnode.add(root);
		tester.iterateTreeForNodesInQueue(1);

		assertEquals(6, tester.queueofnode.peek().value);
	}

	@Test
	public void processTreeHeight() {
		MonkBreakingArrayIntoTree tester = new MonkBreakingArrayIntoTree();
		Node root = new Node(5);
		root.leftsideofnode = new Node(6);

		assertEquals(2, tester.processTreeHeight(root));
	}

	@Test
	public void loadTreeRootForHeight_CorrectIterationOfNode_HeightOfTree() {
		MonkBreakingArrayIntoTree tester = new MonkBreakingArrayIntoTree();
		Node root = new Node(5);
		root.leftsideofnode = new Node(6);
		tester.root = root;

		assertEquals(2, tester.loadTreeRootForHeight());
	}

}
