import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;

public class MonkBreakingArrayIntoTree {

	class Node {
		private int value;
		
		public int getValue() {
			return value;
		}

		Node leftsideofnode, rightsideofnode;

		public Node(int value) {
			this.value = value;
			leftsideofnode = rightsideofnode = null;
		}
	}

	Node root;

	void loadValueForInsertion(int key) {
		root = findLocationToInsertNewNode(root, key);
	}

	Node findLocationToInsertNewNode(Node root, int key) {

		if (root == null) {
			return insertNewNode(key);
		}

		if (key <= root.value)
			root.leftsideofnode = findLocationToInsertNewNode(root.leftsideofnode, key);
		else if (key > root.value)
			root.rightsideofnode = findLocationToInsertNewNode(root.rightsideofnode, key);

		return root;
	}

	Node insertNewNode(int key) {
		root = new Node(key);
		return root;
	}

	Queue<Node> q = new LinkedList<Node>();
	int height;

	int loadTreeRootForHeight() {
		return processTreeHeight(root);
	}

	int processTreeHeight(Node node) {
		if (node == null)
			return 0;

		q.add(node);

		while (true) {

			if (isTreeHeightAvailable())
				return getTreeHeight();

			increaseHeightOfTree();
			
			int noofnodeinqueue = q.size();
			iterateTreeForNodesInQueue(noofnodeinqueue);
		}
	}

	boolean isTreeHeightAvailable() {
		if (q.size() == 0) {
			return true;
		}
		return false;
	}

	int getTreeHeight() {
		return height;
	}

	void increaseHeightOfTree(){
		height++;
	}
	
	void iterateTreeForNodesInQueue(int noofnodeinqueue) {
		while (noofnodeinqueue > 0) {
			Node newnode = q.peek();
			q.remove();
			if (newnode.leftsideofnode != null)
				q.add(newnode.leftsideofnode);
			if (newnode.rightsideofnode != null)
				q.add(newnode.rightsideofnode);
			noofnodeinqueue--;
		}
	}

	public static void main(String[] args) throws NumberFormatException, IOException {
		MonkBreakingArrayIntoTree tree = new MonkBreakingArrayIntoTree();
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int noofnodetoinsert = Integer.parseInt(br.readLine());
		
		String arrayofnodevalue[] = br.readLine().split(" ");
		
		for (int i = 0; i < noofnodetoinsert; i++) {
			tree.loadValueForInsertion(Integer.parseInt(arrayofnodevalue[i]));
		}
		
		System.out.println(tree.loadTreeRootForHeight());
	}
}