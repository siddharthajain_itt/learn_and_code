import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
 
 
public class TestClass {
     
	static int stack[];
	static int top;
	public static void main(String[] args) throws IOException {
		
		StringBuffer output=new StringBuffer();
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        int numberoftestcase=Integer.parseInt(br.readLine());
		
        for(int i=0;i<numberoftestcase;i++)
		{  
			String[] passDetail=br.readLine().split(" ");
			
			stack=new int[Integer.parseInt(passDetail[0])+1];
			top=-1;
			push(Integer.parseInt(passDetail[1]));
			for(int j=0;j<Integer.parseInt(passDetail[0]);j++)
			{
				String passes=br.readLine();
				if(passes.charAt(0)=='B')
				{
					pushB();
				}
				else
				   push(Integer.parseInt(passes.substring(2)));
			    
			}
			System.out.println("Player "+stack[top]);
		}
	}
 
	private static void pushB() {
		stack[++top]=stack[top-2];
	}
 
	private static void push(int parseInt) {
		stack[++top]=parseInt;
	}
	}