
import java.util.*;
import java.io.*;

class SurveyStatistics {
	static BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	static CustomHashMap<String, Integer> sportpopularityhashmap = new CustomHashMap<String, Integer>();
	static CustomHashMap<String, Integer> nameofpeoplesurveyed = new CustomHashMap<String, Integer>();
	static int numberofpeoplelikefootball;

	public static void main(String args[]) throws IOException {

		int numberofpeopletosurvey = Integer.parseInt(bufferedReader.readLine());

		if (!IsInputValid(numberofpeopletosurvey)) {
			return;
		}

		if (!surveyForFavouriteSport(numberofpeopletosurvey)) {
			return;
		}

		String mostlovedsport = findFavouriteSport(sportpopularityhashmap);

		System.out.println(mostlovedsport);
		System.out.println(numberofpeoplelikefootball);
	}

	public static boolean surveyForFavouriteSport(int numberOfPeopleForSurvey) throws IOException {
		for (int indexOfPeople = 0; indexOfPeople < numberOfPeopleForSurvey; indexOfPeople++) {
			String array[] = bufferedReader.readLine().split(" ");
			String namePerson = array[0];
			String favSport = array[1];
			if (!IsNameValid(namePerson, favSport)) {
				return false;
			}
			if (favSport.equals("football")) {
				numberofpeoplelikefootball++;
			}
			if (!isPersonAlreadySurveyed(namePerson)) {
				addSurveyRecord(favSport);
				addPersonNameInSurveyedList(namePerson);
			}
		}
		return true;
	}

	public static boolean isPersonAlreadySurveyed(String nameOfPerson) {
		return nameofpeoplesurveyed.containsKey(nameOfPerson);
	}

	public static void addPersonNameInSurveyedList(String nameOfPerson) {
		nameofpeoplesurveyed.put(nameOfPerson, 1);
	}

	public static void addSurveyRecord(String sportsName) {
		if (sportpopularityhashmap.containsKey(sportsName)) {
			int totalNumberOfPlayers = sportpopularityhashmap.get(sportsName);
			sportpopularityhashmap.put(sportsName, ++totalNumberOfPlayers);
		} else {
			sportpopularityhashmap.put(sportsName, 1);
		}
	}

	public static String findFavouriteSport(CustomHashMap<String, Integer> surveyDetails) {
		String mostLovedSport = "";
		int numberOfPeople = 0;
		for (CustomHashMap.Tupple<String, Integer> entrysetofsportpopularityhashmap : sportpopularityhashmap
				.entrySet()) {
			if (entrysetofsportpopularityhashmap.value == numberOfPeople) {
				if (!(entrysetofsportpopularityhashmap.key.compareTo(mostLovedSport) > 0)) {
					mostLovedSport = entrysetofsportpopularityhashmap.key;
					numberOfPeople = entrysetofsportpopularityhashmap.value;
				}
			} else if (entrysetofsportpopularityhashmap.value > numberOfPeople) {
				mostLovedSport = entrysetofsportpopularityhashmap.key;
				numberOfPeople = entrysetofsportpopularityhashmap.value;

			}
		}
		return mostLovedSport;
	}

	public static boolean IsInputValid(int numberOfEntries) {
		if (numberOfEntries >= 1 && numberOfEntries <= 100000)
			return true;
		else
			return false;
	}

	public static boolean IsNameValid(String peopleName, String sportsName) {
		if ((peopleName.length() >= 1 && peopleName.length() <= 10)
				&& (sportsName.length() >= 1 && sportsName.length() <= 10))
			return true;
		else
			return false;
	}
}

class CustomHashMap<K, V> {
	private Tupple<K, V>[] buckets;
	private int capacity = 100;

	static class Tupple<K, V> {
		K key;
		V value;
		Tupple<K, V> next;

		public Tupple(K key, V value, Tupple<K, V> next) {
			this.key = key;
			this.value = value;
			this.next = next;
		}
	}

	@SuppressWarnings("unchecked")
	public CustomHashMap() {
		buckets = new Tupple[capacity];
	}

	public void put(K newKey, V data) {

		int hash = generateHashCodeForKey(newKey);
		Tupple<K, V> newEntry = new Tupple<K, V>(newKey, data, null);

		if (buckets[hash] == null) {
			buckets[hash] = newEntry;
		} else {
			Tupple<K, V> previous = null;
			Tupple<K, V> current = buckets[hash];

			while (current != null) {
				if (current.key.equals(newKey)) {
					if (previous == null) {
						newEntry.next = current.next;
						buckets[hash] = newEntry;
						return;
					} else {
						newEntry.next = current.next;
						previous.next = newEntry;
						return;
					}
				}
				previous = current;
				current = current.next;
			}
			previous.next = newEntry;
		}
	}

	public V get(K key) {
		int hash = generateHashCodeForKey(key);
		if (buckets[hash] == null) {
			return null;
		} else {
			Tupple<K, V> temp = buckets[hash];
			while (temp != null) {
				if (temp.key.equals(key)) {
					return temp.value;
				}
				temp = temp.next;
			}
			return null;
		}
	}

	public boolean remove(K deleteKey) {
		int hash = generateHashCodeForKey(deleteKey);
		if (buckets[hash] == null) {
			return false;
		} else {
			Tupple<K, V> previous = null;
			Tupple<K, V> current = buckets[hash];

			while (current != null) {
				if (current.key.equals(deleteKey)) {
					if (previous == null) {
						buckets[hash] = buckets[hash].next;
						return true;
					} else {
						previous.next = current.next;
						return true;
					}
				}
				previous = current;
				current = current.next;
			}
			return false;
		}

	}

	public void display() {
		for (int i = 0; i < capacity; i++) {
			if (buckets[i] != null) {
				Tupple<K, V> entry = buckets[i];
				while (entry != null) {
					System.out.print("{" + entry.key + "=" + entry.value + "}" + " ");
					entry = entry.next;
				}
			}
		}
	}

	public boolean containsKey(K keytofind) {
		int index = generateHashCodeForKey(keytofind);
		Tupple<K, V> temp = buckets[index];
		while (temp != null) {
			if (temp.key.equals(keytofind)) {
				return true;
			}
			temp = temp.next;
		}
		return false;
	}

	public Set<Tupple<K, V>> entrySet() {
		Set<Tupple<K, V>> set = new HashSet<Tupple<K, V>>();
		for (int i = 0; i < capacity; i++) {
			if (buckets[i] != null) {
				Tupple<K, V> entry = buckets[i];
				while (entry != null) {
					set.add(entry);
					entry = entry.next;
				}
			}
		}
		return set;
	}

	private int generateHashCodeForKey(K key) {
		return Math.abs(key.hashCode()) % capacity;
	}

}
