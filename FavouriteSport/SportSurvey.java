import java.util.*;
import java.io.*;

class SportSurvey {
	static BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	static HashMap<String, Integer> sportpopularityhashmap = new HashMap<String, Integer>();
	static int numberofpeoplelikefootball;

	public static void main(String args[]) throws Exception {
		int numberofpeopletosurvey = Integer.parseInt(bufferedReader.readLine());
		if(!IsInputValid(numberofpeopletosurvey))
		{
			return;
		}
		
		if(!surveyForFavouriteSport(numberofpeopletosurvey)){
			return;
		}
		String mostlovedsport = findFavouriteSport(sportpopularityhashmap);
		System.out.println(mostlovedsport);
		System.out.println(numberofpeoplelikefootball);
	}

	public static boolean surveyForFavouriteSport(int numberOfPeopleForSurvey) throws IOException {
		for (int indexOfPeople = 0; indexOfPeople < numberOfPeopleForSurvey; indexOfPeople++) {
			String array[] = bufferedReader.readLine().split(" ");
			String namePerson = array[0];
			String favSport = array[1];
			if(!IsNameValid(namePerson,favSport))
			{
				return false;
			}
			if (favSport.equals("football")) {
				numberofpeoplelikefootball++;
			}
			addSurveyRecord(favSport);
		}
		return true;
	}

	public static void addSurveyRecord(String sportsName) {
		if (sportpopularityhashmap.containsKey(sportsName)) {
			int totalNumberOfPlayers = sportpopularityhashmap.get(sportsName);
			sportpopularityhashmap.put(sportsName, ++totalNumberOfPlayers);
		} else {
			sportpopularityhashmap.put(sportsName, 1);
		}
	}

	public static String findFavouriteSport(HashMap<String, Integer> surveyDetails) {
		String mostLovedSport = null;
		int numberOfPeople = 0;
		for (Map.Entry<String, Integer> entrysetofsportpopularityhashmap : sportpopularityhashmap.entrySet()) {
			if (entrysetofsportpopularityhashmap.getValue() > numberOfPeople) {
				mostLovedSport = entrysetofsportpopularityhashmap.getKey();
				numberOfPeople = entrysetofsportpopularityhashmap.getValue();
			}
		}
		return mostLovedSport;
	}
	
	public static boolean IsInputValid(int numberOfEntries)
	{
		if(numberOfEntries >= 1 && numberOfEntries <= 100000)
			return true;
		else
			return false;
 	}
 
	public static boolean IsNameValid(String peopleName, String sportsName)
	{
		if((peopleName.length() >= 1 && peopleName.length() <= 10) && (sportsName.length() >= 1 && sportsName.length() <= 10))
			return true;
		else
			return false;
 	}
}
