import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Stack;

public class TestClass {

	public static void main(String argsp[]) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int testCases = Integer.parseInt(br.readLine());
		int results[] = new int[testCases];

		for (int i = 0; i < testCases; i++) {
			String input[] = br.readLine().split(" ");
			int passes = Integer.parseInt(input[0]);
			int currentPlayer = Integer.parseInt(input[1]);
			Stack<Integer> passLog = new Stack<Integer>();
			passLog.push(currentPlayer);
			int lastPosition = Integer.MIN_VALUE;
			String lastPassType = null;
			int position = -1;
			for (int j = 0; j < passes; j++) {
				String tupple[] = br.readLine().split(" ");
				String passType = tupple[0];

				if (tupple.length > 1)
					position = Integer.parseInt(tupple[1]);
				if (passType.equals("P"))
					passLog.push(position);
				else if (lastPassType.equals("B") && passType.equals("B") && passLog.lastElement() != lastPosition)
					passLog.push(lastPosition);
				else
					lastPosition = passLog.pop();
				lastPassType = passType;
			}

			results[i] = passLog.pop();
		}

		for (int i : results)
			System.out.println("Player " + i);
	}
}