public class CustomHashMap<K, V> {
	private Tupple<K, V>[] buckets;
	private int capacity = 4;

	static class Tupple<K, V> {
		K key;
		V value;
		Tupple<K, V> next;

		public Tupple(K key, V value, Tupple<K, V> next) {
			this.key = key;
			this.value = value;
			this.next = next;
		}
	}

	@SuppressWarnings("unchecked")
	public CustomHashMap() {
		buckets = new Tupple[capacity];
	}

	public void put(K newKey, V data) {

		int hash = generateHashCodeForKey(newKey);
		Tupple<K, V> newEntry = new Tupple<K, V>(newKey, data, null);

		if (buckets[hash] == null) {
			buckets[hash] = newEntry;
		} else {
			Tupple<K, V> previous = null;
			Tupple<K, V> current = buckets[hash];

			while (current != null) { 
				if (current.key.equals(newKey)) {
					if (previous == null) { // node has to be insert on first of
											// bucket.
						newEntry.next = current.next;
						buckets[hash] = newEntry;
						return;
					} else {
						newEntry.next = current.next;
						previous.next = newEntry;
						return;
					}
				}
				previous = current;
				current = current.next;
			}
			previous.next = newEntry;
		}
	}

	public V get(K key) {
		int hash = generateHashCodeForKey(key);
		if (buckets[hash] == null) {
			return null;
		} else {
			Tupple<K, V> temp = buckets[hash];
			while (temp != null) {
				if (temp.key.equals(key))
					return temp.value;
				temp = temp.next; // return value corresponding to key.
			}
			return null; // returns null if key is not found.
		}
	}

	public boolean remove(K deleteKey) {
		int hash = generateHashCodeForKey(deleteKey);
		if (buckets[hash] == null) {
			return false;
		} else {
			Tupple<K, V> previous = null;
			Tupple<K, V> current = buckets[hash];

			while (current != null) { // we have reached last entry node of
										// bucket.
				if (current.key.equals(deleteKey)) {
					if (previous == null) { // delete first entry node.
						buckets[hash] = buckets[hash].next;
						return true;
					} else {
						previous.next = current.next;
						return true;
					}
				}
				previous = current;
				current = current.next;
			}
			return false;
		}

	}

	public void display() {
		for (int i = 0; i < capacity; i++) {
			if (buckets[i] != null) {
				Tupple<K, V> entry = buckets[i];
				while (entry != null) {
					System.out.print("{" + entry.key + "=" + entry.value + "}" + " ");
					entry = entry.next;
				}
			}
		}
	}

	private int generateHashCodeForKey(K key) {
		return Math.abs(key.hashCode()) % capacity;
	}

}