import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;

import org.junit.Test;

public class TestSumAlternateArrayValue {
	// Naming Convention: MethodName_StateUnderTest_ExpectedBehavior

	@Test
	public void loadArrayData_LoadDataInArray_ReturnValueFromArray() throws Exception {
		// Arrange
		SumAlternateArrayValue altvalue = new SumAlternateArrayValue();
		String input = "1 2 3 4 5 6 7 8 9";
		InputStream in = new ByteArrayInputStream(input.getBytes());
		
		//Act
		System.setIn(in);

		//Assert
		assertEquals(1, altvalue.loadArrayData()[0][0]);
		assertEquals(2, altvalue.loadArrayData()[0][1]);
		assertEquals(3, altvalue.loadArrayData()[0][2]);
		assertEquals(4, altvalue.loadArrayData()[1][0]);
		assertEquals(5, altvalue.loadArrayData()[1][1]);
		assertEquals(6, altvalue.loadArrayData()[1][2]);
		assertEquals(7, altvalue.loadArrayData()[2][0]);
		assertEquals(8, altvalue.loadArrayData()[2][1]);
		assertEquals(9, altvalue.loadArrayData()[2][2]);
	}

	@Test(expected = NumberFormatException.class)
	public void getAndLoadArrayData_LoadInvalidDataTypeInArray_ThrowNumberFormatException() throws Exception {
		//Arrange
		SumAlternateArrayValue altvalue = new SumAlternateArrayValue();
		String input = "sid 2 3 4 5 6 7 8 9";
		InputStream in = new ByteArrayInputStream(input.getBytes(Charset.defaultCharset()));
		
		//Act
		System.setIn(in);
		
		//Assert
		altvalue.loadArrayData();
	}

	@Test(expected = ArrayIndexOutOfBoundsException.class)
	public void getAndLoadArrayData_AccessInvalidIndex_ThrowArrayIndexOutOfBoundException() throws Exception {
		//Arrange
		SumAlternateArrayValue altvalue = new SumAlternateArrayValue();
		String input = "1 2 3 4 5 6 7 8 9";
		InputStream in = new ByteArrayInputStream(input.getBytes(Charset.defaultCharset()));
		
		//Act
		System.setIn(in);
		
		//Assert
		assertEquals(1, altvalue.loadArrayData()[0][4]);
	}

	@Test(expected = ArrayIndexOutOfBoundsException.class)
	public void getAndLoadArrayData_InputLessNumberOfValues_ThrowArrayIndexOutOfBoundException() throws Exception {
		//Arrange
		SumAlternateArrayValue altvalue = new SumAlternateArrayValue();
		String input = "1 2 3 4 5 6 7 8";
		InputStream in = new ByteArrayInputStream(input.getBytes(Charset.defaultCharset()));
		
		//Act
		System.setIn(in);
		
		//Assert
		altvalue.loadArrayData();
	}

	@Test
	public void identifyAlternateArrayValue_CalculateSumOfAlternateValue_SumOfAlternateValue() throws Exception {
		//Arrange
		int[][] array = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
		
		//Act
		SumAlternateArrayValue altvalue = new SumAlternateArrayValue();
		
		//Assert
		assertEquals(25, altvalue.identifyAlternateArrayValue(array)[0]);
		assertEquals(20, altvalue.identifyAlternateArrayValue(array)[1]);
	}

	@Test(expected = ArrayIndexOutOfBoundsException.class)
	public void identifyAlternateArrayValue_LessNumberOfInputInArray_ThrowArrayIndexOutOfBoundException()
			throws Exception {
		//Arrange
		int[][] array = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8 } };
		
		//Act
		SumAlternateArrayValue altvalue = new SumAlternateArrayValue();
		
		//Assert
		altvalue.identifyAlternateArrayValue(array);
	}

	@Test(expected = ArrayIndexOutOfBoundsException.class)
	public void displayResult_LoadLessNumberOfValues_ThrowArrayIndexOutOfBoundException() throws Exception {
		//Arrange
		int[] array = { 1 };
		
		//Act
		SumAlternateArrayValue altvalue = new SumAlternateArrayValue();
		
		//Assert
		altvalue.displayResult(array);
	}

}
