import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Deque;
import java.util.LinkedList;

class ChristieFriendLinkedList {
	static BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(System.in));
	static Deque<Integer> friendqueue = new LinkedList<Integer>();

	public static void main(String args[]) throws Exception {
		String[] friendnumber = null;

		int nooftestcase = Integer.parseInt(bufferedreader.readLine());
		if (!numberOfTestCaseValidation(nooftestcase)) {
			return;
		}

		for (int j = 0; j < nooftestcase; j++) {
			friendnumber = bufferedreader.readLine().split(" ");
			int totalfriend = Integer.parseInt(friendnumber[0]);
			int totalfriendtobedeleted = Integer.parseInt(friendnumber[1]);
			if (!numberOfFriendValidation(totalfriend) && !numberOFFriendToBeDeleted(totalfriendtobedeleted)) {
				return;
			}
			if (!friendRemoval(totalfriendtobedeleted, totalfriend)) {
				return;
			}
			finalFriendListDisplay();
		}
	}

	public static boolean friendRemoval(int totalfriendtobedeleted, int totalfriend) throws IOException {
		String friendPopularityArray[] = bufferedreader.readLine().split(" ");
		int currentvalueofdeletedfriend = 0;

		for (int i = 0; i < totalfriend; i++) {
			int currentFriendPopularity = Integer.parseInt(friendPopularityArray[i]);
			if (!popularityOfFriend(currentFriendPopularity)) {
				return false;
			}
			if (currentvalueofdeletedfriend < totalfriendtobedeleted) {
				while (!friendqueue.isEmpty() && friendqueue.getLast() < currentFriendPopularity
						&& currentvalueofdeletedfriend < totalfriendtobedeleted) {
					friendqueue.removeLast();
					currentvalueofdeletedfriend++;
				}
			}
			friendqueue.addLast(currentFriendPopularity);
		}
		return true;
	}

	public static void finalFriendListDisplay() {
		while (!friendqueue.isEmpty()) {
			System.out.print(friendqueue.removeFirst() + " ");
		}
		System.out.println();
	}

	public static boolean numberOfTestCaseValidation(int numberoftestcase) {
		if (1 <= numberoftestcase && numberoftestcase <= 1000) {
			return true;
		}
		return false;
	}

	public static boolean numberOfFriendValidation(int numberoffriendvalidation) {
		if (1 <= numberoffriendvalidation && numberoffriendvalidation <= 100000) {
			return true;
		}
		return false;
	}

	public static boolean numberOFFriendToBeDeleted(int numberoffriendtobedeleted) {
		if (1 <= numberoffriendtobedeleted && numberoffriendtobedeleted <= 100000) {
			return true;
		}
		return false;
	}

	public static boolean popularityOfFriend(int popularityoffriend) {
		if (1 <= popularityoffriend && popularityoffriend <= 100000) {
			return true;
		}
		return false;
	}
}
