import java.io.BufferedReader;
import java.io.IOException;
import java.io.*;
import java.io.InputStreamReader;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedList;

class ChristieFriendList {
	static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	static Deque<Integer> friendqueue = new LinkedList<Integer>();

	public static void main(String args[]) throws Exception {
		String[] friendnumbers = null;
		int nooftestcases = Integer.parseInt(br.readLine());
		for (int j = 0; j < nooftestcases; j++) {
			friendnumbers = br.readLine().split(" ");
			int totalfriends = Integer.parseInt(friendnumbers[0]);
			int totalfriendstobedeleted = Integer.parseInt(friendnumbers[1]);
			friendRemoval(totalfriendstobedeleted, totalfriends);
			finalFriendListDisplay();
		}
	}

	public static void friendRemoval(int totalfriendstobedeleted, int totalfriends) throws IOException {
		String friendPopularityArray[] = br.readLine().split(" ");
		int currentvalueofdeletedfriends = 0;

		for (int i = 0; i < totalfriends; i++) {
			int currentFriendPopularity = Integer.parseInt(friendPopularityArray[i]);
			if (currentvalueofdeletedfriends < totalfriendstobedeleted) {
				while (!friendqueue.isEmpty() && friendqueue.getLast() < currentFriendPopularity
						&& currentvalueofdeletedfriends < totalfriendstobedeleted) {
					friendqueue.removeLast();
					currentvalueofdeletedfriends++;
				}
			}
			friendqueue.addLast(currentFriendPopularity);
		}
	}

	public static void finalFriendListDisplay() {
		while (!friendqueue.isEmpty()) {
			System.out.print(friendqueue.removeFirst() + " ");
		}
		System.out.println();
	}
}
