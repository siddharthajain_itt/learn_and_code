import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;

public class TestClass {
	static LinkedList<Spider> spiderqueue = new LinkedList<Spider>();

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String input[] = br.readLine().split(" ");
		int queuesize = 0;
		int noofelementtodequeue = 0;
		TestClass testclass = new TestClass();
			noofelementtodequeue = testclass.dequeueSizeValidation(Integer.parseInt(input[1]));
			queuesize = testclass.inputSizeValidation(Integer.parseInt(input[0]), Integer.parseInt(input[1]));

		testclass.queueCreator(br, queuesize);

		for (int i = 0; i < noofelementtodequeue; i++) {
			int index = testclass.getIndexOfMaxPower(noofelementtodequeue);
			testclass.removeMaxPower(index, noofelementtodequeue);
			System.out.print(index + " ");
		}

	}

	public int getIndexOfMaxPower(int x) {
		x = (x < spiderqueue.size()) ? x : spiderqueue.size();
		int maxpower = Integer.MIN_VALUE;
		int indexofmaxpower = Integer.MIN_VALUE;
		for (int i = 0; i < x; i++) {
			Spider q = spiderqueue.get(i);
			if (q.getPower() > maxpower) {
				indexofmaxpower = q.getIndex();
				maxpower = q.getPower();
			}
		}
		return indexofmaxpower;
	}

	public void removeMaxPower(int index, int x) {
		x = (x < spiderqueue.size()) ? x : spiderqueue.size();
		for (int i = 0; i < x; i++) {
			Spider q = spiderqueue.remove();
			if (q.getIndex() != index) {
				if (q.getPower() == 0) {
					q.setValue(0);
				} else {
					q.setValue(q.getPower() - 1);
				}
				spiderqueue.addLast(q);
			}
		}
	}

	public void queueCreator(BufferedReader br, int sizeofinputarray) {
		try {
			String value[] = br.readLine().split(" ");
			for (int i = 0; i < sizeofinputarray; i++) {
				Spider q = new Spider((i + 1), Integer.parseInt(value[i]));
				spiderqueue.add(q);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public int dequeueSizeValidation(int dequeuesize) {
		if (dequeuesize < 1 || dequeuesize > 316) {
			throw new IllegalArgumentException("Invalid Dequeusize");
		}
		return dequeuesize;
	}

	public int inputSizeValidation(int sizeofinputarray, int dequeuesize) {
		if (sizeofinputarray < dequeuesize || sizeofinputarray > (dequeuesize * dequeuesize)) {
			throw new IllegalArgumentException("Invalid inputsize");
		}
		return sizeofinputarray;
	}

}

class Spider {
	private int index, power;

	public Spider(int index, int power) {
		this.index = index;
		this.power = power;
	}

	public int getIndex() {
		return index;
	}

	public int getPower() {
		return power;
	}

	public void setValue(int power) {
		this.power = power;
	}
}