import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;

class SpiderPower {
	static LinkedList<Spider> spiderqueue = new LinkedList<Spider>();

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String input[] = br.readLine().split(" ");
		int queuesize = 0;
		int noofelementtodequeue = 0;

		SpiderPower spiderpower = new SpiderPower();
		noofelementtodequeue = spiderpower.dequeueSizeValidation(Integer.parseInt(input[1]));
		queuesize = spiderpower.inputSizeValidation(Integer.parseInt(input[0]), Integer.parseInt(input[1]));

		spiderpower.queueCreator(br, queuesize);

		for (int iterationnumber = 0; iterationnumber < noofelementtodequeue; iterationnumber++) {
			int index = spiderpower.getIndexOfMaxPower(noofelementtodequeue);
			spiderpower.removeMaxPower(index, noofelementtodequeue);
			System.out.print(index + " ");
		}

	}

	public int getIndexOfMaxPower(int noofiteration) {
		noofiteration = (noofiteration < spiderqueue.size()) ? noofiteration : spiderqueue.size();
		int maxpower = Integer.MIN_VALUE;
		int indexofmaxpower = Integer.MIN_VALUE;
		for (int spiderretrieveindex = 0; spiderretrieveindex < noofiteration; spiderretrieveindex++) {
			Spider dequeuedspider = spiderqueue.get(spiderretrieveindex);
			if (dequeuedspider.getPower() > maxpower) {
				indexofmaxpower = dequeuedspider.getIndex();
				maxpower = dequeuedspider.getPower();
			}
		}
		return indexofmaxpower;
	}

	public void removeMaxPower(int index, int noofiteration) {
		noofiteration = (noofiteration < spiderqueue.size()) ? noofiteration : spiderqueue.size();
		for (int spiderindex = 0; spiderindex < noofiteration; spiderindex++) {
			Spider dequeuedspider = spiderqueue.remove();
			if (dequeuedspider.getIndex() != index) {
				if (dequeuedspider.getPower() == 0) {
					dequeuedspider.setValue(0);
				} else {
					dequeuedspider.setValue(dequeuedspider.getPower() - 1);
				}
				spiderqueue.addLast(dequeuedspider);
			}
		}
	}

	public void queueCreator(BufferedReader br, int sizeofinputarray) {
		try {
			String value[] = br.readLine().split(" ");
			for (int i = 0; i < sizeofinputarray; i++) {
				Spider newspider = new Spider((i + 1), Integer.parseInt(value[i]));
				spiderqueue.add(newspider);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public int dequeueSizeValidation(int dequeuesize) {
		if (dequeuesize < 1 || dequeuesize > 316) {
			throw new IllegalArgumentException("Invalid Dequeusize");
		}
		return dequeuesize;
	}

	public int inputSizeValidation(int sizeofinputarray, int dequeuesize) {
		if (sizeofinputarray < dequeuesize || sizeofinputarray > (dequeuesize * dequeuesize)) {
			throw new IllegalArgumentException("Invalid inputsize");
		}
		return sizeofinputarray;
	}

}

class Spider {
	private int index, power;

	public Spider(int index, int power) {
		this.index = index;
		this.power = power;
	}

	public int getIndex() {
		return index;
	}

	public int getPower() {
		return power;
	}

	public void setValue(int power) {
		this.power = power;
	}
}